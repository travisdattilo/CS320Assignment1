#include "prog1_2.h"
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>

int main(int argc, char *argv[]){
    printf("Assignment #1-3, Travis Dattilo, travisdattilo@yahoo.com\n");
    if(argc != 2){
        printf("\nThis program expects a single command line argument.\n"); //Error message
        return 0;
    }

    int N = atoi(argv[1]);                          
    STACK* stack = MakeStack(N);            //Creates a stack the with the size of the single command line argument

    char input[256];  
    int num;
    char exit[100] = "exit";
    char push[100] = "push";
    char pop[100] = "pop";
    char *token;
    char *token1;
    char *token2;
    int tokenCount;
            
    do{
        tokenCount = 0;                     //clearing token   count for next input
        memset(input, 0, 256);              //clearing current input for next input
        printf("> ");         
        scanf(" %[^\n]",input);   
        
        token = strtok(input," ");
        token1 = token;

        while(token != NULL){               //Checks total number of tokens entered by user
            tokenCount++;
            token = strtok(NULL," ");
            if(tokenCount == 1){
                token2 = token;
            }
        }   
        
        if(tokenCount == 2 && strcmp(push,token1) == 0){        //Checks if user is trying to push something    
            if((*token2) < 0x39 && (*token2) > 0x30){           //Checks if item to be pushed is a number
                num = atoi(token2);          
                Push(stack,num);
            }
        }
        else if(tokenCount == 1 && strcmp(pop,token1) == 0){    //Checks if user is trying to pop                                   
                    int i = Pop(stack);
                    printf("%d\n", i);
        }
    }
    while(N-- > 1 && strcmp(exit, input) != 0);                 //to end program execution
    
    return 0;
}
