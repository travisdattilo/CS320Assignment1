#include "prog1_2.h"
#include <stdio.h>
#include <stdlib.h>

STACK* MakeStack(int initialCapacity){
    STACK* stack;
    stack = (STACK*) malloc (1 * sizeof(STACK));
    stack->capacity = initialCapacity;
    stack->size = 0;
    stack->data = (int*) malloc (stack->capacity * sizeof(int));
    return stack;
}

int isEmpty(STACK* stackPtr){
    return stackPtr->size == 0;
}

int isFull(STACK* stackPtr){
    return stackPtr->size == stackPtr->capacity - 1;
}

void Push(STACK *stackPtr, int data){
    if(isFull(stackPtr)){                   //Doubles capacity when pushing onto a full stack              
        Grow(stackPtr);
    }
    stackPtr->data[stackPtr->size] = data;  //Pushes data onto the stack
    stackPtr->size++;   
}

int Pop(STACK *stackPtr){   
    if(isEmpty(stackPtr)){                  //Cannot pop off of an empty stack, returns -1
        return -1;
    }
    stackPtr->size--; 
    return stackPtr->data[stackPtr->size];
}

void Grow(STACK *stackPtr){                 //Increases capacity of stack (doubles it) 
    int *tmp;
    int tmpcapacity;
    tmpcapacity = stackPtr->capacity * 2;
    tmp = stackPtr->data;
    stackPtr->data = (int*) malloc (tmpcapacity * sizeof(int)); 
    for(int i = 0; i < stackPtr->capacity; i++){    
        stackPtr->data[i] = tmp[i];
    }
    stackPtr->capacity = tmpcapacity;   
}


