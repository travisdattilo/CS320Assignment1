Travis Dattilo  
travisdattilo@yahoo.com  

"prog1_1.c"  
This simple C program prompts the user to enter their full name, and prints a greeting using that full name. 

"prog1_2.h"  
This header file contains type definitions for a stack structure, which includes typical stack function headers.

"prog1_2.c"  
This program uses the header file for the actual implementation of the stack functions including "MakeStack" to 
create the stack, "Push" and "Pop" for data manipulation in the stack, and "Grow" to double the capacity of the 
stack when pushing data onto the stack when it is full. Note: this program has no entry points and is only for
code for stack functions.

"prog1_3.c"  
This Driver program takes a single command line argument following the name of the program being executed. As 
stated in the instructions, the single command line argument entered will be an integer (if not, a helpful error
message is printed). Following that the user may enter push and pop commands with as many spaces as they would like
in between each token entered. Note: The user may enter as many commands as the number entered for the single command
line argument integer.
